#!/bin/bash
echo -n 'пример' > ./in.txt || exit 1
openssl enc -base64 -d <<< 'dchEsKOm7cdxW/LAPovvEe2iv6LUOlF4q1XpNSbxc61/0dCpm83liVBGnsTTufa2VWqslYEqyXlQFYOPXcLznTBYqTON8laskgd9LK/QXzUKnE3pErmzrgZ8dmGct5HLElTbuuja7iRAaBGB6ka6G8a6dZDfQfGEYtx+QEGg+og3TSSZdP9HAVeFlKmQOp9j5AY/r3q4ys0aWAMceIuR9C9vQ3q8h3WA53AxT5lmwhhP7QzIDwFY3oWj0ZpukleLQYAT1QN5rer0Q6ThsnO5DsxtnI5y1tLOIemsvRtoqUXRBFJSBO6g3im03XkvLzg9BerDJvwydGCmpTa9PFchYA==' > ./sig.txt || exit 1
read -r -d '' pub << EOF
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnQtaNCjDySvY1Jci9LvA
zaDUezB23i/NlHS43OWaYfI+bdJo0q2PPcopF2Ii9cuR5AacxyYQjncckGdPgnsO
G7U3/cazeFUBF3O20SX9sV8Lgy5BOMrsnlmn4u9RDrViF67Vaxm+DNfBs1l51lCa
JTIEzMzlmdkeNyS/Y/KqGDKxCLlTMiR3NRj1W9QWAuFj3U/4MgUv4dw7k4UJIKvv
BuLTxxxLXuHVz8DvIbs0fRe5Ab7hS0J9Hxhd4K45nAjpKO+8YNkpAk9kMMDTDGXs
BRKgvIQ1GbIle0LCVWCfKJYY07C18SGfGw9ACNHeQFvZkZwfh9J5XLvhTMNFhfc/
PQIDAQAB
-----END PUBLIC KEY-----
EOF
echo -n "$pub" > pub.pem || exit 1
openssl dgst -sha256 -verify pub.pem -signature sig.txt in.txt
